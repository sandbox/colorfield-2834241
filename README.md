# REST import

Creates and updates existing Drupal 8 storage on a regular base, from a REST datasource.

It provides the tools for easily importing contents from other applications via a web service endpoint. The approach is to use inheritance of converters instead of configuration via YAML (this is the main difference with the Migrate style).

Also, it differentiates from [External Entities](https://www.drupal.org/project/external_entities) by
- Storing entities as a standalone version (so the web service can be unavailable, only update will suffer).
- Using plain Entities (Content type and Node, Vocabulary and Taxonomy term, ...) so it abstracts the difficulty of supporting other modules implementation (Search API, Pathauto, Views, ...) or caching invalidation and focuses on field conversion support (like Paragraphs, ...).

Currently, only JSON is supported, further development can include XML and HAL.

The scope of the operations are create, update, translate and delete.

It is based on a mapping defined in in the EntityMapper class that
- maps a source entity (e.g. "news") and a target
Drupal 8 entity type and entity id, also known as "bundle" (e.g. "node" and "article").
- defines a converter class, each external entity can be converted into a Drupal entity by defining a class that implements
the EntityConverterInterface.

## JSON model definition

The proposed web service definition is based on [jsonapi.org](http://jsonapi.org/).
Further explanations can be found in the json directory.

The import can be triggered by a cron or manually. Each request to import resources
are then enqueued via the Queue API.

## Getting started

### Install and configure

1. Enable the rest_import module
2. Configure the web service (/admin/config/rest_import/web_service), see config below or get the active one from configuration management.
3. Make sure that the web service is reachable

### Create entity mapping

Copy and rename the rest_import.entity_mapper.example.yml in rest_import.entity_mapper.yml
and define the mapping

### Create converters

Create classes for each Converter and define the field mapping in each.

@todo documentation about field converter, field collection converter

## One time import (manual)

Use manual operations

1. Populate the queue by entity, respect the sequence: /admin/rest_import/operations
2. Click on Process queue or process the queue using drush `drush queue-run manual_entity_process`

## Regular import (cron)

This handles regular import based on update of each entity, each time.
It triggers a sequence of entity types for each language then enqueue the results and execute the queue via a cron worker.

## Diff import

This import method relies on bi-directional web service exchange : the web service waits for confirmation of import then provides only the items that were not imported yet.
The action passed for each entity can the be create, update or delete.

It is not currently being implemented.

## Example of web service configuration

- Endpoint: http://my.domain.org/webservice
- Debug endpoint: http://my.local.dev/modules/custom/rest_import/json
- Debug mode: No
- Limit per batch: 0
- Default language: en
- Import user: admin
- Format: JSON
